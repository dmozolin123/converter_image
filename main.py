import sys
import random
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QVBoxLayout, QMainWindow, QPushButton, QHBoxLayout, QFileDialog, QInputDialog, QFrame
from PyQt5.QtGui import QImage, QPixmap, QColor


class StartWindows(QWidget):
    """
    Создаем основную форму
    """

    def __init__(self, main_windows, *args, **kwargs) -> None:
        super().__init__(main_windows, *args, **kwargs)
        self.main_windows = main_windows
        self.setStyleSheet("""
            QLabel{font-size: 20px;}
            QPushButton{font-size:15px;}
        """)
        self.main_layout = QVBoxLayout()
        self.set_widgets()
        self.setLayout(self.main_layout)

    def set_widgets(self):
        label = QLabel(
            "<center>Добро пожаловать!</center>\n<center>Вырерите действие</center>")
        button_paint = QPushButton(text="Открыть редактор")
        button_paint.clicked.connect(self.main_windows.windows_paint)
        button_exit = QPushButton(text="Выход")
        button_exit.clicked.connect(lambda: sys.exit(1))
        layout_button = QHBoxLayout()
        layout_button.addWidget(button_paint)
        layout_button.addWidget(button_exit)
        self.main_layout.addWidget(label)
        self.main_layout.addLayout(layout_button)


class ImageConstruct(QImage):

    def sepia(self):
        depth, ok = QInputDialog.getInt(
            QWidget(), 'Диалоговое окно', 'Введите число для сепии')
        if not ok:
            depth = 5
        for i in range(self.width()):
            for j in range(self.height()):
                a = self.pixelColor(i, j).red()
                b = self.pixelColor(i, j).green()
                c = self.pixelColor(i, j).blue()
                S = (a + b + c) // 3
                a = S + depth * 2
                b = S + depth
                c = S
                if (a > 255):
                    a = 255
                if (b > 255):
                    b = 255
                if (c > 255):
                    c = 255
                self.setPixelColor(i, j, QColor(a, b, c))
        return self

    def negotive(self):
        for i in range(self.width()):
            for j in range(self.height()):
                a = self.pixelColor(i, j).red()
                b = self.pixelColor(i, j).green()
                c = self.pixelColor(i, j).blue()
                self.setPixelColor(i, j, QColor(255 - a, 255 - b, 255 - c))
        return self

    def brightness(self):
        factor, ok = QInputDialog.getInt(
            QWidget(), 'Диалоговое окно', 'Введите число для яркости')
        if not ok:
            factor = 5
        for i in range(self.width()):
            for j in range(self.height()):
                a = self.pixelColor(i, j).red() + factor
                b = self.pixelColor(i, j).green() + factor
                c = self.pixelColor(i, j).blue() + factor
                if (a < 0):
                    a = 0
                if (b < 0):
                    b = 0
                if (c < 0):
                    c = 0
                if (a > 255):
                    a = 255
                if (b > 255):
                    b = 255
                if (c > 255):
                    c = 255
                self.setPixelColor(i, j, QColor(a, b, c))
        return self

    def noise(self):
        factor, ok = QInputDialog.getInt(
            QWidget(), 'Диалоговое окно', 'Введите число для шума')
        if not ok:
            factor = 5
        for i in range(self.width()):
            for j in range(self.height()):
                rand = random.randint(-factor, factor)
                a = self.pixelColor(i, j).red() + rand
                b = self.pixelColor(i, j).green() + rand
                c = self.pixelColor(i, j).blue() + rand
                if (a < 0):
                    a = 0
                if (b < 0):
                    b = 0
                if (c < 0):
                    c = 0
                if (a > 255):
                    a = 255
                if (b > 255):
                    b = 255
                if (c > 255):
                    c = 255
                self.setPixelColor(i, j, QColor(a, b, c))
        return self

    def black_white(self):
        factor, ok = QInputDialog.getInt(
            QWidget(), 'Диалоговое окно', 'Введите число для ЧБ')
        if not ok:
            factor = 5
        for i in range(self.width()):
            for j in range(self.height()):
                a = self.pixelColor(i, j).red()
                b = self.pixelColor(i, j).green()
                c = self.pixelColor(i, j).blue()
                S = a + b + c
                if (S > (((255 + factor) // 2) * 3)):
                    a, b, c = 255, 255, 255
                else:
                    a, b, c = 0, 0, 0
                self.setPixelColor(i, j, QColor(a, b, c))
        return self

    def to_gray(self):
        for i in range(self.width()):
            for j in range(self.height()):
                result = 0.299 * self.pixelColor(i, j).red() + 0.587 * self.pixelColor(
                    i, j).green() + 0.114 * self.pixelColor(i, j).blue()
                self.setPixelColor(i, j,  QColor(
                    round(result), round(result), round(result)))
        return self

    def to_bin(self):
        for i in range(self.width()):
            for j in range(self.height()):
                if (self.pixelColor(i, j).red() + self.pixelColor(i, j).green() + self.pixelColor(i, j).blue()) > (int((255 + 100) // 2) * 3):
                    self.setPixelColor(i, j,  QColor(0, 0, 0))
                else:
                    self.setPixelColor(i, j,  QColor(255, 255, 255))
        return self


class PaintWindows(QWidget):
    def __init__(self, widget, *args, **kwargs) -> None:
        super().__init__(widget, *args, **kwargs)
        self.setStyleSheet("""
            QLabel{
                font-size: 20px;
            }
            QPushButton{
                font-size: 15px;
            }
        """)
        self.setMinimumSize(800, 500)
        self.main_layout = QHBoxLayout()
        self.set_left_layout()
        self.set_right_layout()
        self.frame_right.hide()
        self.setLayout(self.main_layout)

    def set_left_layout(self):
        self.label_img = QLabel()
        self.label_dir_img = QLabel()
        button_open_img = QPushButton("Открыть изображение")
        button_open_img.clicked.connect(self.open_img)
        self.button_save_img = QPushButton('Сохранить изображение')
        self.button_save_img.clicked.connect(self.save_img)
        layout = QVBoxLayout()
        layout.addWidget(self.label_img)
        layout.addWidget(self.label_dir_img)
        layout.addWidget(button_open_img)
        layout.addWidget(self.button_save_img)
        self.button_save_img.hide()
        self.main_layout.addLayout(layout)

    def set_label_img(self, img):
        pixmap_image = QPixmap(QPixmap.fromImage(img))
        self.label_img.setPixmap(pixmap_image)

    def open_img(self):
        fname = QFileDialog.getOpenFileName(
            self, 'Открыть файл',  None, "Image (*.png *.jpg *.jpeg)")[0]
        if fname:
            self.qImg = ImageConstruct(fname)
            self.set_label_img(self.qImg)
            self.label_dir_img.setText(fname)
            self.frame_right.show()
            self.button_save_img.show()
        return 0

    def save_img(self):
        fname = QFileDialog.getSaveFileName(
            self, 'Открыть файл',  None, "Image (*.jpg *.jpeg)")[0]
        self.qImg.save(fname, format='jpeg')

    def set_right_layout(self):
        label_command = QLabel("Действия над изображением")

        sepia_button = QPushButton("Сепия изображениея")
        sepia_button.clicked.connect(
            lambda: self.set_label_img(self.qImg.sepia()))

        negative_button = QPushButton("Преобразование в негатив")
        negative_button.clicked.connect(
            lambda: self.set_label_img(self.qImg.negotive()))

        brightness_button = QPushButton("Яркость над изображением")
        brightness_button.clicked.connect(
            lambda: self.set_label_img(self.qImg.brightness()))

        noise_button = QPushButton("Шум в изображении")
        noise_button.clicked.connect(
            lambda: self.set_label_img(self.qImg.noise()))

        black_white_button = QPushButton("Преображение в черно-белое")
        black_white_button.clicked.connect(
            lambda: self.set_label_img(self.qImg.black_white()))

        gray_button = QPushButton("Обесцвечивание изображени")
        gray_button.clicked.connect(
            lambda: self.set_label_img(self.qImg.to_gray()))

        binar_button = QPushButton("Бинаризация изображения")
        binar_button.clicked.connect(
            lambda: self.set_label_img(self.qImg.to_bin()))
        layout = QVBoxLayout()
        layout.addWidget(label_command)
        layout.addWidget(sepia_button)
        layout.addWidget(negative_button)
        layout.addWidget(brightness_button)
        layout.addWidget(noise_button)
        layout.addWidget(black_white_button)
        layout.addWidget(gray_button)
        layout.addWidget(binar_button)
        self.frame_right = QFrame()
        self.frame_right.setLayout(layout)
        self.main_layout.addWidget(self.frame_right)


class MainWidows(QMainWindow):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.start_windows()

    def start_windows(self):

        widget = StartWindows(self)

        self.setCentralWidget(widget)

    def windows_paint(self):
        widget = PaintWindows(self)
        self.setCentralWidget(widget)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    q = MainWidows()
    q.show()
    sys.exit(app.exec_())
